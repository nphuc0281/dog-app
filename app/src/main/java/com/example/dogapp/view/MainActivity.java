package com.example.dogapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.example.dogapp.adapter.GridviewAdapter;
import com.example.dogapp.databinding.ActivityMainBinding;
import com.example.dogapp.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding binding;
    MainViewModel viewModel;
    private GridviewAdapter gridviewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View viewRoot = binding.getRoot();
        setContentView(viewRoot);

        binding.rcDogs.setLayoutManager(new GridLayoutManager(this,2));

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.getListDog().observe(this, list -> {
            gridviewAdapter = new GridviewAdapter(list);
            binding.rcDogs.setAdapter(gridviewAdapter);
        });

    }
}