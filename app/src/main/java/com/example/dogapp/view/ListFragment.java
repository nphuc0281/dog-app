package com.example.dogapp.view;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.dogapp.R;
import com.example.dogapp.adapter.GridviewAdapter;
import com.example.dogapp.databinding.FragmentListBinding;
import com.example.dogapp.viewmodel.MainViewModel;


public class ListFragment extends Fragment {
    private FragmentListBinding binding;
    MainViewModel viewModel;
    private GridviewAdapter gridviewAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.rvDogs.setLayoutManager(new GridLayoutManager(getContext(),2));

        viewModel = new ViewModelProvider(this).get(MainViewModel.class);
        viewModel.getListDog().observe(getViewLifecycleOwner(), list -> {
            gridviewAdapter = new GridviewAdapter(list);
            binding.rvDogs.setAdapter(gridviewAdapter);
        });

    }
}