package com.example.dogapp.adapter;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.example.dogapp.R;
import com.example.dogapp.databinding.DogCardBinding;
import com.example.dogapp.model.Dog;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class GridviewAdapter
        extends RecyclerSwipeAdapter<GridviewAdapter.GridHolder>
        implements Filterable {
    public List<Dog> listDog;
    public List<Dog> listFilterDog;

    public GridviewAdapter(List<Dog> listDog) {
        this.listDog = listDog;
        this.listFilterDog = new ArrayList<>(listDog);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.dog_card;
    }

    public static class GridHolder extends RecyclerView.ViewHolder
    {
        private final DogCardBinding binding;
        public GridHolder(@NonNull DogCardBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
            binding.dogCard.addDrag(
                    SwipeLayout.DragEdge.Right, itemView.findViewById(R.id.dog_card_back));
        }
    }

    @NonNull
    @Override
    public GridHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        DogCardBinding itemBinding = DogCardBinding.inflate(
                LayoutInflater.from(parent.getContext()), parent, false);
        return new GridHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull GridHolder holder, int position) {
        holder.binding.dogCardFront.tvName.setText(listDog.get(position).getName());
        holder.binding.dogCardFront.tvBredFor.setText(listDog.get(position).getBredFor());
        Picasso.get().load(
                listDog.get(position).getUrl()).into(holder.binding.dogCardFront.ivCover);

        holder.binding.dogCardFront.ivFavorite.setBackgroundResource(
                listDog.get(position).isFavorite() ?
                        R.drawable.ic_baseline_favorite_24
                        : R.drawable.ic_baseline_favorite_border_24);

        holder.binding.dogCardFront.ivFavorite.setOnClickListener(e ->{
            if(listDog.get(position).isFavorite()){
                listDog.get(position).setFavorite(false);
                holder.binding.dogCardFront.ivFavorite.setBackgroundResource(
                        R.drawable.ic_baseline_favorite_border_24);
            }else{
                listDog.get(position).setFavorite(true);
                holder.binding.dogCardFront.ivFavorite.setBackgroundResource(
                        R.drawable.ic_baseline_favorite_24);
            }
        });

        holder.binding.dogCardFront.cvFront.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dog dog = listDog.get(holder.getAdapterPosition());
                Bundle bundle = new Bundle();
                bundle.putSerializable("dog",dog);
                Navigation.findNavController(view).navigate(R.id.detailFragment,bundle);
            }
        });

        holder.binding.dogCardBack.cvBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Dog dog = listDog.get(holder.getAdapterPosition());
                Bundle bundle = new Bundle();
                bundle.putSerializable("dog",dog);
                Navigation.findNavController(view).navigate(R.id.detailFragment,bundle);
            }
        });

        holder.binding.dogCardBack.tvDogName.setText(listFilterDog.get(position).getName());
        holder.binding.dogCardBack.tvOrigin.setText(listFilterDog.get(position).getOrigin());
        holder.binding.dogCardBack.tvTemperament.setText(
                listFilterDog.get(position).getTemperament());
        holder.binding.dogCardBack.tvLifespan.setText(listFilterDog.get(position).getLifeSpan());
    }

    @Override
    public int getItemCount() {
        return listDog.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                List<Dog> filterDogs = new ArrayList<>();
                if(constraint.toString().isEmpty()){
                    filterDogs.addAll(listDog);

                }else{
                    for(Dog record : listDog) {
                        if ((record.getName().toLowerCase().contains(
                                constraint.toString().toLowerCase()))) {
                            filterDogs.add(record);
                        }
                    }
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = filterDogs;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listFilterDog.clear();
                listFilterDog.addAll((List<Dog>) results.values);
                notifyDataSetChanged();
            }
        };
    }
}
