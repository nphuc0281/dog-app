package com.example.dogapp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Dog implements Serializable {
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("life_span")
    private String lifeSpan;

    @SerializedName("origin")
    private String origin;

    @SerializedName("bred_for")
    private String bredFor;

    @SerializedName("url")
    private String url;

    @SerializedName("height")
    private Height height;

    @SerializedName("weight")
    private Weight weight;

    @SerializedName("temperament")
    private String temperament;

    private boolean isFavorite;

    public Dog(int id, String name, String imageUrl, String bredFor,
            Height height, Weight weight, String temperament, String lifeSpan, String origin) {
        this.id = id;
        this.name = name;
        this.url = imageUrl;
        this.origin = origin;
        this.isFavorite = false;
        this.bredFor = bredFor;
        this.isFavorite = false;
        this.height = height;
        this.weight = weight;
        this.temperament = temperament;
        this.lifeSpan = lifeSpan;
        this.origin = origin;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getBredFor() {
        return bredFor;
    }

    public void setBredFor(String bred_for) {
        this.bredFor = bred_for;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLifeSpan() {
        return lifeSpan;
    }

    public void setLifeSpan(String lifeSpan) {
        this.lifeSpan = lifeSpan;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Height getHeight() {
        return height;
    }

    public void setHeight(Height height) {
        this.height = height;
    }

    public Weight getWeight() {
        return weight;
    }

    public void setWeight(Weight weight) {
        this.weight = weight;
    }

    public String getTemperament() {
        return temperament;
    }

    public void setTemperament(String temperament) {
        this.temperament = temperament;
    }
}
