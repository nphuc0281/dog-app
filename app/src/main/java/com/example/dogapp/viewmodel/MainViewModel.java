package com.example.dogapp.viewmodel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.dogapp.model.Dog;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.observers.DisposableSingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {
    private DogAPIService dogAPIService;
    private MutableLiveData<List<Dog>> listDog;

    public MutableLiveData<List<Dog>> getListDog(){
        if (listDog == null){
            listDog = new MutableLiveData<List<Dog>>();
            dogAPIService = new DogAPIService();
            dogAPIService.getDogs()
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(new DisposableSingleObserver<List<Dog>>() {
                        @Override
                        public void onSuccess(@NonNull List<Dog> dogs) {
                            listDog.setValue(dogs);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {
                            listDog.setValue(new ArrayList<>());
                        }
                    });
        }
        return listDog;
    }
}
